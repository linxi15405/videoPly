package com.video.action;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.alibaba.fastjson.JSON;
import com.video.bean.Video;
import com.video.videoImpl.VideoImpl;

public class VideoAction extends DispatchAction {
	
	public void toGetVideo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("########toGetVideo########");
		Map<String, Object> map=new HashMap<String, Object>();
		String callbackparam=request.getParameter("callbackparam");
		String json=null;
		VideoImpl v =new VideoImpl();
		Video vd=new Video();
		int count=0;
		try {
			vd=v.getVideo();
			map.put("video", vd);
			count=v.count();
			map.put("count", count);
			json = JSON.toJSONString(map);
			PrintWriter writer=response.getWriter();
			writer.write(callbackparam+"("+json+")");
		} catch (Exception e) {
			System.out.println(e);
		}finally{
			System.out.println("########end toGetVideo########");
		}
	}
	
	public String toFindVideo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("########toFindVideo########");
		Map<String, Object> map=new HashMap<String, Object>();
		String callbackparam=request.getParameter("callbackparam");
		String json=null;
		VideoImpl v =new VideoImpl();
		Video vd=new Video();
		String count=request.getParameter("count");
		try {
			vd=v.findByCount(Integer.parseInt(count)-1);
			map.put("video", vd);
			json = JSON.toJSONString(map);
			PrintWriter writer=response.getWriter();
			writer.write(callbackparam+"("+json+")");
		} catch (Exception e) {
			System.out.println(e);
		}finally{
			System.out.println("########end toFindVideo########");
		}
		return null;
	}
	
	public String toNextVideo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("########toNextVideo########");
		Map<String, Object> map=new HashMap<String, Object>();
		String callbackparam=request.getParameter("callbackparam");
		String json=null;
		VideoImpl v =new VideoImpl();
		Video vd=new Video();
		String id=request.getParameter("id");
		try {
			vd=v.nextVideo(Integer.parseInt(id));
			map.put("video", vd);
			json = JSON.toJSONString(map);
			PrintWriter writer=response.getWriter();
			writer.write(callbackparam+"("+json+")");
		} catch (Exception e) {
			System.out.println(e);
		}finally{
			System.out.println("########end toNextVideo########");
		}
		return null;
	}
	
	public String toLastVideo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println("########toNextVideo########");
		Map<String, Object> map=new HashMap<String, Object>();
		String callbackparam=request.getParameter("callbackparam");
		String json=null;
		VideoImpl v =new VideoImpl();
		Video vd=new Video();
		String id=request.getParameter("id");
		try {
			vd=v.lastVideo(Integer.parseInt(id));
			map.put("video", vd);
			json = JSON.toJSONString(map);
			PrintWriter writer=response.getWriter();
			writer.write(callbackparam+"("+json+")");
			System.out.println("########toNextVideo########");
		} catch (Exception e) {
			System.out.println(e);
		}finally{
			System.out.println("########end toNextVideo########");
		}
		return null;
	}
}
