package com.video.bean;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shiji-cx on 2017/7/11.
 */
public class BaseBean implements java.io.Serializable {
	/**
	 * 获取查询字段
	 * */
	public String selectClos() {
		StringBuffer str = new StringBuffer();
		// 获得自己的属性
		Field[] fields = this.getClass().getDeclaredFields();
		// 获得父类的属性
		Field[] superFields = this.getClass().getSuperclass()
				.getDeclaredFields();
		// 自己的和父类的属性相加
		Field[] allFields = addFields(fields, superFields);
		for (Field field : allFields) {
			str.append("`");
			str.append(field.getName());
			str.append("`,");
		}
		return str.toString().substring(0, str.length() - 1);
	}

	/**
	 * 相加f1和f2的Field
	 *
	 * @param f1
	 * @param f2
	 * @return
	 */
	private Field[] addFields(Field[] f1, Field[] f2) {
		List<Field> fields = new ArrayList<>();
		for (Field field : f1) {
			fields.add(field);
		}
		for (Field field : f2) {
			fields.add(field);
		}
		return fields.toArray(new Field[f1.length + f2.length]);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
