package com.video.bean;

import java.io.Serializable;

public class MallLogBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Object itemType;
	private Object item_id;
	private Object itemNum;

	public Object getItemType() {
		return itemType;
	}

	public void setItemType(Object itemType) {
		this.itemType = itemType;
	}

	public Object getItem_id() {
		return item_id;
	}

	public void setItem_id(Object itemId) {
		item_id = itemId;
	}

	public Object getItemNum() {
		return itemNum;
	}

	public void setItemNum(Object itemNum) {
		this.itemNum = itemNum;
	}

}
