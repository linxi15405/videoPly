package com.video.bean;

public class ServerBean implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	/** 服务器ID */
	private int server_id;
	/** 服务器名称 */
	private String server_desc;
	/** 平台ID */
	private int plat_id;
	/** 平台名称 */
	private String plat_desc;
	/** 标记 */
	private int mark;

	public ServerBean() {
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public int getServer_id() {
		return server_id;
	}

	public void setServer_id(int serverId) {
		server_id = serverId;
	}

	public String getServer_desc() {
		return server_desc;
	}

	public void setServer_desc(String serverDesc) {
		server_desc = serverDesc;
	}

	public int getPlat_id() {
		return plat_id;
	}

	public void setPlat_id(int platId) {
		plat_id = platId;
	}

	public String getPlat_desc() {
		return plat_desc;
	}

	public void setPlat_desc(String platDesc) {
		plat_desc = platDesc;
	}

}
