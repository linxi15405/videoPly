package com.video.bean;

import java.io.Serializable;

/**
 * 查询BEAN
 * 
 * @author ALLEN
 */
public class QueryBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Object platform; // 渠道ID
	private Object platFormName; // 渠道名称
	private Object serverId; // 服务器ID
	private Object serverName; // 服务器名
	private Object channelID; // 渠道ID
	private Object channelName; // 渠道名称
	private Object statisticTime; // 统计时间
	private Object dailyLogon; // 每日登陆用户数
	private Object dailyReg; // 新增注册数
	private Object macFirst; // 新增MAC数
	private Object newRegPay; // 新增用户付费率
	private Object dailyPayRatio;// 每日登陆用户付费率
	private Object arpuRatio; // ARPU-付费总额/活跃用户
	private Object arppuRatio; // ARPPU-付费总额/付费人数
	private Object totalPay; // 付费总额
	private Object ltv_1Ratio; // LTV_1
	private Object ltv_3Ratio; // LTV_3
	private Object ltv_7Ratio; // LTV_7
	private Object ltv_14Ratio; // LTV_14
	private Object ltv_30Ratio; // LTV_30
	private Object payPlayer; // 充值账号数
	private Object unRepeatPay; // 不重复充值账号数

	private Object playerId; // 角色ID
	private Object platformUID; // 平台ID
	private Object order_id; // 订单号
	private Object product_id; // 产品编号
	private Object goodPrice; // 充值金额
	private Object operState; // 操作状态
	private Object maxOnline; // 最高在线
	private Object avgOnline; // 平均在线
	private Object realOnline;// 实时在线
	private Object buyPlayerNum; // 购买账号数
	private Object buyProductNum; // 商品购买数

	private Object count0; // >0 && <=6
	private Object count6;
	private Object count1h;
	private Object count3h;
	private Object count5h;
	private Object count1k;
	private Object count2k;
	private Object count5k;
	private Object count10k;

	private int vipLv_0; // VIP_LEVEL
	private int vipLv_1;
	private int vipLv_2;
	private int vipLv_3;
	private int vipLv_4;
	private int vipLv_5;
	private int vipLv_6;
	private int vipLv_7;
	private int vipLv_8;
	private int vipLv_9;
	private int vipLv_10;
	private int vipLv_11;
	private int vipLv_12;
	private int vipLv_13;
	private int vipLv_14;
	private int vipLv_15;

	public QueryBean() {
	}

	public QueryBean(int vipLv_0, int vipLv_1, int vipLv_2, int vipLv_3,
			int vipLv_4, int vipLv_5, int vipLv_6, int vipLv_7, int vipLv_8,
			int vipLv_9, int vipLv_10, int vipLv_11, int vipLv_12,
			int vipLv_13, int vipLv_14, int vipLv_15) {
		this.vipLv_0 = vipLv_0;
		this.vipLv_1 = vipLv_1;
		this.vipLv_2 = vipLv_2;
		this.vipLv_3 = vipLv_3;
		this.vipLv_4 = vipLv_4;
		this.vipLv_5 = vipLv_5;
		this.vipLv_6 = vipLv_6;
		this.vipLv_7 = vipLv_7;
		this.vipLv_8 = vipLv_8;
		this.vipLv_9 = vipLv_9;
		this.vipLv_10 = vipLv_10;
		this.vipLv_11 = vipLv_11;
		this.vipLv_12 = vipLv_12;
		this.vipLv_13 = vipLv_13;
		this.vipLv_14 = vipLv_14;
		this.vipLv_15 = vipLv_15;
	}

	public Object getBuyPlayerNum() {
		return buyPlayerNum;
	}

	public void setBuyPlayerNum(Object buyPlayerNum) {
		this.buyPlayerNum = buyPlayerNum;
	}

	public Object getBuyProductNum() {
		return buyProductNum;
	}

	public void setBuyProductNum(Object buyProductNum) {
		this.buyProductNum = buyProductNum;
	}

	public int getVipLv_0() {
		return vipLv_0;
	}

	public void setVipLv_0(int vipLv_0) {
		this.vipLv_0 = vipLv_0;
	}

	public int getVipLv_1() {
		return vipLv_1;
	}

	public void setVipLv_1(int vipLv_1) {
		this.vipLv_1 = vipLv_1;
	}

	public int getVipLv_2() {
		return vipLv_2;
	}

	public void setVipLv_2(int vipLv_2) {
		this.vipLv_2 = vipLv_2;
	}

	public int getVipLv_3() {
		return vipLv_3;
	}

	public void setVipLv_3(int vipLv_3) {
		this.vipLv_3 = vipLv_3;
	}

	public int getVipLv_4() {
		return vipLv_4;
	}

	public void setVipLv_4(int vipLv_4) {
		this.vipLv_4 = vipLv_4;
	}

	public int getVipLv_5() {
		return vipLv_5;
	}

	public void setVipLv_5(int vipLv_5) {
		this.vipLv_5 = vipLv_5;
	}

	public int getVipLv_6() {
		return vipLv_6;
	}

	public void setVipLv_6(int vipLv_6) {
		this.vipLv_6 = vipLv_6;
	}

	public int getVipLv_7() {
		return vipLv_7;
	}

	public void setVipLv_7(int vipLv_7) {
		this.vipLv_7 = vipLv_7;
	}

	public int getVipLv_8() {
		return vipLv_8;
	}

	public void setVipLv_8(int vipLv_8) {
		this.vipLv_8 = vipLv_8;
	}

	public int getVipLv_9() {
		return vipLv_9;
	}

	public void setVipLv_9(int vipLv_9) {
		this.vipLv_9 = vipLv_9;
	}

	public int getVipLv_10() {
		return vipLv_10;
	}

	public void setVipLv_10(int vipLv_10) {
		this.vipLv_10 = vipLv_10;
	}

	public int getVipLv_11() {
		return vipLv_11;
	}

	public void setVipLv_11(int vipLv_11) {
		this.vipLv_11 = vipLv_11;
	}

	public int getVipLv_12() {
		return vipLv_12;
	}

	public void setVipLv_12(int vipLv_12) {
		this.vipLv_12 = vipLv_12;
	}

	public int getVipLv_13() {
		return vipLv_13;
	}

	public void setVipLv_13(int vipLv_13) {
		this.vipLv_13 = vipLv_13;
	}

	public int getVipLv_14() {
		return vipLv_14;
	}

	public void setVipLv_14(int vipLv_14) {
		this.vipLv_14 = vipLv_14;
	}

	public int getVipLv_15() {
		return vipLv_15;
	}

	public void setVipLv_15(int vipLv_15) {
		this.vipLv_15 = vipLv_15;
	}

	public Object getRealOnline() {
		return realOnline;
	}

	public void setRealOnline(Object realOnline) {
		this.realOnline = realOnline;
	}

	public Object getMaxOnline() {
		return maxOnline;
	}

	public void setMaxOnline(Object maxOnline) {
		this.maxOnline = maxOnline;
	}

	public Object getAvgOnline() {
		return avgOnline;
	}

	public void setAvgOnline(Object avgOnline) {
		this.avgOnline = avgOnline;
	}

	public Object getCount0() {
		return count0;
	}

	public void setCount0(Object count0) {
		this.count0 = count0;
	}

	public Object getCount6() {
		return count6;
	}

	public void setCount6(Object count6) {
		this.count6 = count6;
	}

	public Object getCount1h() {
		return count1h;
	}

	public void setCount1h(Object count1h) {
		this.count1h = count1h;
	}

	public Object getCount3h() {
		return count3h;
	}

	public void setCount3h(Object count3h) {
		this.count3h = count3h;
	}

	public Object getCount5h() {
		return count5h;
	}

	public void setCount5h(Object count5h) {
		this.count5h = count5h;
	}

	public Object getCount1k() {
		return count1k;
	}

	public void setCount1k(Object count1k) {
		this.count1k = count1k;
	}

	public Object getCount2k() {
		return count2k;
	}

	public void setCount2k(Object count2k) {
		this.count2k = count2k;
	}

	public Object getCount5k() {
		return count5k;
	}

	public void setCount5k(Object count5k) {
		this.count5k = count5k;
	}

	public Object getCount10k() {
		return count10k;
	}

	public void setCount10k(Object count10k) {
		this.count10k = count10k;
	}

	public Object getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Object playerId) {
		this.playerId = playerId;
	}

	public Object getPlatformUID() {
		return platformUID;
	}

	public void setPlatformUID(Object platformUID) {
		this.platformUID = platformUID;
	}

	public Object getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Object orderId) {
		order_id = orderId;
	}

	public Object getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Object productId) {
		product_id = productId;
	}

	public Object getGoodPrice() {
		return goodPrice;
	}

	public void setGoodPrice(Object goodPrice) {
		this.goodPrice = goodPrice;
	}

	public Object getOperState() {
		return operState;
	}

	public void setOperState(Object operState) {
		this.operState = operState;
	}

	public Object getPayPlayer() {
		return payPlayer;
	}

	public void setPayPlayer(Object payPlayer) {
		this.payPlayer = payPlayer;
	}

	public Object getUnRepeatPay() {
		return unRepeatPay;
	}

	public void setUnRepeatPay(Object unRepeatPay) {
		this.unRepeatPay = unRepeatPay;
	}

	public Object getLtv_1Ratio() {
		return ltv_1Ratio;
	}

	public void setLtv_1Ratio(Object ltv_1Ratio) {
		this.ltv_1Ratio = ltv_1Ratio;
	}

	public Object getLtv_3Ratio() {
		return ltv_3Ratio;
	}

	public void setLtv_3Ratio(Object ltv_3Ratio) {
		this.ltv_3Ratio = ltv_3Ratio;
	}

	public Object getLtv_7Ratio() {
		return ltv_7Ratio;
	}

	public void setLtv_7Ratio(Object ltv_7Ratio) {
		this.ltv_7Ratio = ltv_7Ratio;
	}

	public Object getLtv_14Ratio() {
		return ltv_14Ratio;
	}

	public void setLtv_14Ratio(Object ltv_14Ratio) {
		this.ltv_14Ratio = ltv_14Ratio;
	}

	public Object getLtv_30Ratio() {
		return ltv_30Ratio;
	}

	public void setLtv_30Ratio(Object ltv_30Ratio) {
		this.ltv_30Ratio = ltv_30Ratio;
	}

	public Object getStatisticTime() {
		return statisticTime;
	}

	public void setStatisticTime(Object statisticTime) {
		this.statisticTime = statisticTime;
	}

	public Object getDailyLogon() {
		return dailyLogon;
	}

	public void setDailyLogon(Object dailyLogon) {
		this.dailyLogon = dailyLogon;
	}

	public Object getDailyReg() {
		return dailyReg;
	}

	public void setDailyReg(Object dailyReg) {
		this.dailyReg = dailyReg;
	}

	public Object getMacFirst() {
		return macFirst;
	}

	public void setMacFirst(Object macFirst) {
		this.macFirst = macFirst;
	}

	public Object getNewRegPay() {
		return newRegPay;
	}

	public void setNewRegPay(Object newRegPay) {
		this.newRegPay = newRegPay;
	}

	public Object getDailyPayRatio() {
		return dailyPayRatio;
	}

	public void setDailyPayRatio(Object dailyPayRatio) {
		this.dailyPayRatio = dailyPayRatio;
	}

	public Object getArpuRatio() {
		return arpuRatio;
	}

	public void setArpuRatio(Object arpuRatio) {
		this.arpuRatio = arpuRatio;
	}

	public Object getArppuRatio() {
		return arppuRatio;
	}

	public void setArppuRatio(Object arppuRatio) {
		this.arppuRatio = arppuRatio;
	}

	public Object getTotalPay() {
		return totalPay;
	}

	public void setTotalPay(Object totalPay) {
		this.totalPay = totalPay;
	}

	public Object getChannelID() {
		return channelID;
	}

	public void setChannelID(Object channelID) {
		this.channelID = channelID;
	}

	public Object getChannelName() {
		return channelName;
	}

	public void setChannelName(Object channelName) {
		this.channelName = channelName;
	}

	public Object getPlatform() {
		return platform;
	}

	public void setPlatform(Object platform) {
		this.platform = platform;
	}

	public Object getPlatFormName() {
		return platFormName;
	}

	public void setPlatFormName(Object platFormName) {
		this.platFormName = platFormName;
	}

	public Object getServerId() {
		return serverId;
	}

	public void setServerId(Object serverId) {
		this.serverId = serverId;
	}

	public Object getServerName() {
		return serverName;
	}

	public void setServerName(Object serverName) {
		this.serverName = serverName;
	}

}