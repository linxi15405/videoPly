package com.video.bean;

public class DBSetting implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	public String poolname; // 连接池名称

	public String dbname; // 数据名称

	public String drivername; // 驱动名

	public String username; // DB 用户名

	public String userpassword; // DB 密码

	public String dburl; // 数据库连接url

	public String getDbname() {
		return dbname;
	}

	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

	public String getDburl() {
		return dburl;
	}

	public void setDburl(String dburl) {
		this.dburl = dburl;
	}

	public String getDrivername() {
		return drivername;
	}

	public void setDrivername(String drivername) {
		this.drivername = drivername;
	}

	public String getPoolname() {
		return poolname;
	}

	public void setPoolname(String poolname) {
		this.poolname = poolname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserpassword() {
		return userpassword;
	}

	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}

}
