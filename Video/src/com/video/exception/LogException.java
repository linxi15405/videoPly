package com.video.exception;

import com.video.init.LogUtils;

public class LogException extends RuntimeException implements
		java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LogException() {
		super();
	}

	public static void print(Exception exc, String memo) {
		StackTraceElement[] stacks = exc.getStackTrace();

		StringBuffer loggerstr = new StringBuffer(1000);
		for (int i = 0; i < stacks.length; i++) {
			loggerstr.append("file   name:" + stacks[i].getFileName());
			loggerstr.append("\t");
			loggerstr.append("class:" + stacks[i].getClassName());
			loggerstr.append("\t");
			loggerstr.append("method   name:" + stacks[i].getMethodName());
			loggerstr.append("\t");
			loggerstr.append("Line   No:" + stacks[i].getLineNumber());
			loggerstr.append("\t");
			loggerstr.append("\t");
			loggerstr.append("--------------------");
			loggerstr.append("\n");
		}
		// logger.error(exc);

		Throwable Cause = exc.getCause();
		while (Cause != null) {
			StackTraceElement[] Causestacks = Cause.getStackTrace();
			for (int i = 0; i < Causestacks.length; i++) {
				loggerstr.append("file   name:" + Causestacks[i].getFileName());
				loggerstr.append("\t");
				loggerstr.append("class:" + Causestacks[i].getClassName());
				loggerstr.append("\t");
				loggerstr.append("method   name:"
						+ Causestacks[i].getMethodName());
				loggerstr.append("\t");
				loggerstr.append("Line   No:" + Causestacks[i].getLineNumber());
				loggerstr.append("\t");
				loggerstr.append("--------------------");
				loggerstr.append("\n");
			}
			Cause = Cause.getCause();
		}
		LogUtils.TRACE_INFO(memo);
		LogUtils.TRACE_ERROR(loggerstr.toString(), exc);
	}
}
