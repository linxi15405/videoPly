package com.video.connection;

import java.io.InputStream;
import java.util.Properties;

import com.video.init.LogUtils;
import com.video.exception.LogException;

public class SqlManager {

	private final static Properties prop = new Properties();

	public SqlManager() {
		init();
	}

	public void init() {
		try {
			InputStream is = getClass().getResourceAsStream("sql.properties");
			prop.load(is);
			if (is != null) {
				is.close();
			}
			LogUtils.TRACE_INFO("sql.properties加载...................");
		} catch (Exception e) {
			LogException.print(e, "file sql.properties load failure");
		}
	}

	public static String getprop(String key) {
		String strkey = "";
		if (prop != null) {
			strkey = prop.getProperty(key);
		}

		if (!CheckUtil.checkNull(strkey)) {
			new SqlManager();
			strkey = prop.getProperty(key);
		}
		return strkey;
	}
}
