package com.video.connection;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import oracle.jdbc.OracleResultSet;
import oracle.sql.CLOB;

import org.apache.log4j.Logger;

import com.psc.db.ConnectionPool;
import com.video.listener.*;
import com.video.bean.DBSetting;
import com.video.exception.LogException;

@SuppressWarnings("unchecked")
public class DBMember {

	private static Logger logger = Logger.getLogger(DBMember.class);

	public DBMember() {

	}

	public Connection getConnectBypool(String dbpoolname) throws SQLException {
		Connection conn = null;
		try {
			conn = DBConnectionPool.getPool(dbpoolname).getConnection();
			return conn;
		} catch (SQLException se) {
			throw se;
		}
	}

	/**
	 * 不带参数的结果集
	 * 
	 * @param jndi
	 * @param sql
	 * @return
	 * @throws SQLException
	 * @throws SQLException
	 */

	/*
	 * 获得链接
	 */
	public static Connection getConnectByJNDI(String dbpoolname)
			throws SQLException {

		// synchronized(this){

		Connection conn = null;

		try {

			// 根据不同的连接池名称 获得连接
			long a = System.currentTimeMillis();
			// System.out.println("poolname======"+DBConnectionPool.getPoolBypoolname(dbpoolname));
			ConnectionPool p = DBConnectionPool.getPoolBypoolname(dbpoolname);
			if (p != null) {
				conn = p.getConnection();
			}

			if (conn == null) {
				try {
					if (!DBConnectionPool.existPool(dbpoolname)) {
						logger.info(dbpoolname + "连接池为空重置连接池");
						// 获得连接池的配置信息
						DBSetting dbinfo = XmlUtil.dbsettingMap.get(dbpoolname);
						conn = DBConnectionPool.bulidPool(dbinfo)
								.getConnection();
						if (conn == null)
							return null;
					}
				} catch (Exception e) {
					LogException.print(e, "连接池" + dbpoolname + "检查");
				}
			}
			logger.info("获得连接时间= " + (System.currentTimeMillis() - a) + " 豪秒");
			return conn;
		} catch (SQLException se) {
			throw se;
		}
		// }
	}

	/**
	 * jdbc连接数据库
	 * 
	 * @param url
	 * @return
	 */
	public Connection getConnectByJDBC(String url, String userName,
			String Password) {
		Connection conn = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(url, userName, Password);
			// "jdbc:oracle:thin:@127.0.0.1:1521:XE","Game_member","root"
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public Connection getConnect(String drivername, String url,
			String username, String password) throws Exception {
		Connection conn = null;
		try {
			Class.forName(drivername);
			conn = DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			logger.error("链接失败" + e.getMessage(), e);
			e.printStackTrace();
			throw e;
		}
		return conn;
	}

	/**
	 * @param jndi
	 * @param sql
	 * @param o
	 * @return
	 * @throws SQLException
	 */
	public boolean executSQL(String jndi, String sql, Object[] o)
			throws SQLException {
		boolean re = false;
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnectByJNDI(jndi);
			stmt = conn.prepareStatement(sql);
			for (int i = 0; i < o.length; i++) {
				stmt.setObject(i + 1, o[i]);
			}

			re = stmt.execute();
			re = true;
		} catch (SQLException e) {
			throw e;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return re;
	}

	public boolean executeProcedure(Connection conn, String callProcedureSQL,
			Object[] obj) throws SQLException {
		CallableStatement proc = null;
		if (conn != null) {
			try {
				proc = conn.prepareCall("{ call " + callProcedureSQL + " }");
				if (obj != null) {
					for (int i = 0; i < obj.length; i++) {
						proc.setString(i + 1, obj[i].toString());
					}
				}
				proc.execute();

				return true;
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			} finally {
				if (proc != null) {
					proc.close();
				}
			}
		}
		return false;
	}

	public String executeProcedure(Connection conn, String callProcedureSQL,
			int returnIndex, Object[] obj) throws SQLException {
		CallableStatement proc = null;
		if (conn != null) {
			try {
				proc = conn.prepareCall("{ call " + callProcedureSQL + " }");
				if (obj != null) {
					for (int i = 0; i < obj.length; i++) {
						if (i + 1 < returnIndex) {
							proc.setString(i + 1, obj[i].toString());
						} else {
							proc.registerOutParameter(i + 1, Types.VARCHAR);
						}
					}
				}
				proc.execute();
			} catch (Exception e) {
				throw new SQLException("带返回值的存储过程执行失败" + e.getMessage());
			} finally {
				if (proc != null) {
					proc.close();
				}
			}
		}
		return proc.getString(returnIndex);
	}

	/**
	 * 带事物的sql处理
	 * 
	 * @param conn
	 * @param sql
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public boolean executPSQL(Connection conn, String sql, Object obj[])
			throws SQLException {
		PreparedStatement stmt = null;
		if (conn != null) {
			stmt = conn.prepareStatement(sql);
			try {
				if (obj != null) {
					for (int i = 0; i < obj.length; i++) {
						stmt.setObject(i + 1, obj[i]);
					}
				}

				stmt.execute();
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			} finally {
				if (stmt != null) {
					stmt.close();
				}
			}
		}
		return false;
	}

	/**
	 * 批量数据写入
	 */
	public boolean executeBatch(String jndi, String sql, List<Object[]> list) {
		boolean bool = false;
		PreparedStatement ptmt = null;
		Connection conn = null;
		final int batchSize = 500;
		int count = 0;
		try {
			conn = getConnectByJNDI(jndi);
			conn.setAutoCommit(false);

			if (conn != null) {
				try {
					ptmt = conn.prepareStatement(sql);
					if (list != null && list.size() > 0) {
						for (Object[] obj : list) {
							if (obj != null) {
								for (int i = 0; i < obj.length; i++) {
									ptmt.setObject(i + 1, obj[0]);
								}
								ptmt.addBatch();
								if (++count % batchSize == 0) {
									ptmt.executeBatch();
								}
							}
						}
						ptmt.executeBatch();
						conn.commit();
						bool = true;
					}
				} catch (Exception e1) {
					bool = false;
					conn.rollback();
				} finally {
					ptmt.close();
					conn.close();
					if (list != null) {
						list.clear();
					}
				}
			}
		} catch (Exception e) {
			bool = false;
			LogException.print(e, "批量写入数据报错");
		}
		return bool;
	}

	/**
	 * 批量插入数据
	 * 
	 * @param conn
	 * @param sql
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public static boolean executPSQLBatch(Connection conn, String sql,
			Object obj[]) throws SQLException {
		boolean bool = false;
		PreparedStatement stmt = null;
		if (conn != null) {
			stmt = conn.prepareStatement(sql);
			try {
				if (obj != null) {
					for (int i = 0; i < obj.length; i++) {
						stmt.setObject(i + 1, obj[i]);
					}
				}
				stmt.execute();
				return true;

			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			} finally {
				if (stmt != null) {
					stmt.close();
				}
			}
		}
		return bool;
	}

	/**
	 * 
	 * @param jndi
	 * @param sql
	 * @param o
	 * @return
	 * @throws SQLException
	 */
	public boolean executSQL(String jndi, String sql) throws SQLException {
		boolean re = false;
		PreparedStatement stmt = null;
		Connection conn = null;
		try {
			conn = getConnectByJNDI(jndi);
			stmt = conn.prepareStatement(sql);
			re = stmt.execute();
			re = true;
		} catch (SQLException e) {
			throw e;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return re;
	}

	/**
	 * 带事物的sql处理
	 * 
	 * @param conn
	 * @param sql
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public boolean executPSQL(Connection conn, String sql) throws SQLException {
		PreparedStatement stmt = null;
		if (conn != null) {
			stmt = conn.prepareStatement(sql);
			try {
				stmt.execute();
				return true;

			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			} finally {
				if (stmt != null) {
					stmt.close();
				}
			}
		}
		return false;
	}

	public boolean executPSQL1(Connection conn, String sql, Object obj[])
			throws SQLException {
		PreparedStatement stmt = null;
		if (conn != null) {
			stmt = conn.prepareStatement(sql);
			try {
				if (obj != null) {
					for (int i = 0; i < obj.length; i++) {
						if (i == 1) {
							Blob blob1 = (java.sql.Blob) obj[1];
							// System.out.print("blob1" + blob1);
							stmt.setBlob(i + 1, blob1);
						} else {
							stmt.setObject(i + 1, obj[i]);
						}
					}
				}
				if (stmt.execute())
					return true;
			} catch (SQLException e) {
				e.printStackTrace();
				throw new SQLException();
			} finally {
				if (stmt != null) {
					stmt.close();
				}
			}
		}
		return false;
	}

	/**
	 * 带参数 select语句
	 * 
	 * @param sql
	 * @param o
	 * @return
	 * @throws SQLException
	 */
	public Object getData(String jndi, String sql, Object[] o)
			throws SQLException {
		ResultSet result = null;
		PreparedStatement stmt = null;
		Object obj = null;
		Connection conn = null;
		try {
			conn = getConnectByJNDI(jndi);
			stmt = conn.prepareStatement(sql);

			for (int i = 0; i < o.length; i++) {
				stmt.setObject(i + 1, o[i]);
			}
			result = stmt.executeQuery();
			if (result.next()) {
				obj = result.getObject(1);
			}
			return obj;
		} catch (SQLException me) {
			throw me;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}

	/**
	 * connection 由外界传入
	 * 
	 * @param conn
	 * @param sql
	 * @param o
	 * @return
	 * @throws SQLException
	 */
	public Object getData(Connection conn, String sql, Object[] o)
			throws SQLException {
		ResultSet result = null;
		PreparedStatement stmt = null;
		Object obj = null;
		try {
			stmt = conn.prepareStatement(sql);

			if (o != null) {
				for (int i = 0; i < o.length; i++) {
					stmt.setObject(i + 1, o[i]);
				}
			}
			result = stmt.executeQuery();

			if (result.next()) {
				obj = result.getObject(1);
			}
			return obj;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	/**
	 * 不带参数的结果集
	 * 
	 * @param jndi
	 * @param sql
	 * @return
	 * @throws SQLException
	 * @throws SQLException
	 */
	public Object getData(String jndi, String sql) throws SQLException {
		ResultSet result = null;
		PreparedStatement stmt = null;
		Object obj = null;
		Connection conn = null;
		try {
			conn = getConnectByJNDI(jndi);
			stmt = conn.prepareStatement(sql);

			result = stmt.executeQuery(sql);
			if (result.next()) {
				obj = result.getObject(1);
			}
			return obj;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

	}

	/**
	 * 不带参数的 select
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */

	public List getDataList(String jndi, String sql) throws SQLException {
		ResultSet result = null;
		PreparedStatement stmt = null;
		ArrayList l = new ArrayList();
		Connection conn = null;

		try {
			conn = getConnectByJNDI(jndi);
			if (conn != null) {
				stmt = conn.prepareStatement(sql);
				result = stmt.executeQuery();
				while (result.next()) {
					Object[] o = new Object[result.getMetaData()
							.getColumnCount()];

					for (int i = 0; i < o.length; i++) {
						o[i] = result.getObject(i + 1);
					}
					l.add(o);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			result = null;
			throw e;
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	/**
	 * 带参数的select
	 * 
	 * @param sql
	 * @param obj
	 * @return
	 */
	public List getDataList(String jndi, String sql, Object[] obj)
			throws SQLException {

		ResultSet result = null;
		PreparedStatement stmt = null;
		ArrayList l = new ArrayList();
		Connection conn = null;
		try {
			conn = getConnectByJNDI(jndi);
			if (conn != null) {
				stmt = conn.prepareStatement(sql);

				if (obj != null) {
					for (int i = 0; i < obj.length; i++) {
						stmt.setObject(i + 1, obj[i]);
					}
				}
				result = stmt.executeQuery();
				/*
				 * while (result.next()) { Object[] o = new
				 * Object[result.getMetaData() .getColumnCount()]; for (int i =
				 * 0; i < o.length; i++) {
				 * 
				 * o[i] = result.getObject(i + 1); } l.add(o); }
				 */

				while (result.next()) {
					Object[] o = new Object[result.getMetaData()
							.getColumnCount()];
					for (int i = 0; i < o.length; i++) {
						if (Types.CLOB == result.getMetaData().getColumnType(
								i + 1)) {
							o[i] = result.getString(i + 1);
						} else {
							o[i] = result.getObject(i + 1);
						}
					}
					l.add(o);
				}
			}
		} catch (SQLException e) {
			result = null;
			throw e;
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	public List getDataList(Connection conn, String sql, Object[] obj)
			throws SQLException {

		ResultSet result = null;
		PreparedStatement stmt = null;
		ArrayList l = new ArrayList();
		try {
			if (conn != null) {
				stmt = conn.prepareStatement(sql);

				if (obj != null) {
					for (int i = 0; i < obj.length; i++) {
						stmt.setObject(i + 1, obj[i]);
					}
				}
				result = stmt.executeQuery();

				while (result.next()) {
					Object[] o = new Object[result.getMetaData()
							.getColumnCount()];
					for (int i = 0; i < o.length; i++) {

						o[i] = result.getObject(i + 1);
					}
					l.add(o);
				}
			}
		} catch (SQLException e) {
			result = null;
			throw e;
		} finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	public void disConnect(Connection conn) {
		try {
			if (conn != null && !conn.isClosed())
				conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// //////////////////////////////////////////////////////////////////////////////////////
	/**
	 * conn 由外界传入 方便事物处理
	 * 
	 * @param conn
	 * @param sql
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public boolean executSQLByPool(Connection conn, String sql, Object obj[])
			throws SQLException {
		PreparedStatement stmt = null;

		if (conn != null) {
			stmt = conn.prepareStatement(sql);
			try {
				if (obj != null) {
					for (int i = 0; i < obj.length; i++) {
						stmt.setObject(i + 1, obj[i]);
					}
				}
				int re = stmt.executeUpdate();
				// System.out.println("re=="+re);
				if (re == 1)
					return true;
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			} finally {
				if (stmt != null) {
					stmt.close();
					stmt = null;
				}
			}
		}
		return false;
	}

	public String[] getDBInfoByType(String type, String host, String dbname) {
		String[] s = new String[2];
		if ("MySql".equalsIgnoreCase(type)) {
			s[0] = "com.mysql.jdbc.Driver";
			s[1] = "jdbc:mysql://" + host + "/" + dbname;
		} else if ("SQLServer".equalsIgnoreCase(type)) {
			s[0] = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
			s[1] = "jdbc:microsoft:sqlserver://" + host + ";DataBaseName="
					+ dbname;
		} else if ("Oracle".equalsIgnoreCase(type)) {
			s[0] = "oracle.jdbc.driver.OracleDriver";
			s[1] = "jdbc:oracle:thin:@" + host + ":" + dbname;
		}
		return s;
	}

	@SuppressWarnings("deprecation")
	public static Long execuSQLAndGetSeq(String jndi, String sql, Object obj[],
			String seqName) throws SQLException {
		Connection conn = null;
		conn = getConnectByJNDI(jndi);
		PreparedStatement stmt = null;
		ResultSet result = null;
		Long re_seq = -1L;
		if (conn != null) {
			stmt = conn.prepareStatement(sql);
			try {
				Date startDate = new Date();
				if (obj != null) {
					logger.info("sql=" + sql);
					for (int i = 0; i < obj.length; i++) {
						logger.info("obj[" + i + "]" + obj[i]);
						logger.info("参数" + i + "＝" + obj[i]);
						stmt.setObject(i + 1, obj[i]);
					}
				}

				logger.info("开始时间-------------" + startDate.toLocaleString());
				logger.info(sql);
				boolean state = stmt.execute();
				Date endDate = new Date();
				logger.info("结束时间-------------" + endDate.toLocaleString());
				logger.info("执行秒数-------------"
						+ (endDate.getTime() - startDate.getTime()));
				stmt.close();

				if (!state) {
					String seqsql = "select " + seqName + ".currval from dual";
					stmt = conn.prepareStatement(seqsql);
					result = stmt.executeQuery();
					if (result.next()) {
						re_seq = result.getLong(1);
					}
				}
			} catch (SQLException e) {
				throw e;
			} finally {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
					conn = null;
				}
			}
		}
		return re_seq;
	}

	public static boolean updateCLOB(String jndi, String sqlsel, Object[] obj,
			String sqlupd, Object[] obj2) throws SQLException {

		boolean re = false;

		Connection conn = null;
		PreparedStatement pst1 = null;
		PreparedStatement pst2 = null;
		ResultSet rs = null;

		try {

			conn = getConnectByJNDI(jndi);
			pst1 = conn.prepareStatement(sqlsel);
			for (int i = 0; i < obj.length; i++) {
				pst1.setObject(i + 1, obj[i]);
			}
			logger.info("UpdateCLOB=====开始执行sqlsel= = " + sqlsel);
			rs = pst1.executeQuery();

			if (rs.next()) {
				CLOB clob = ((OracleResultSet) rs).getCLOB(1);
				clob.setString(1, obj2[0].toString());
				pst2 = conn.prepareStatement(sqlupd);
				pst2.setClob(1, clob);
				pst2.setObject(2, obj2[1]);
				logger.info("UPdateCLOB= == =开始执行sqlupdf===" + sqlupd);
				if (pst2.executeUpdate() > 0)
					;
				re = true;
				logger.info("UpdateCLOB=====执行sqlupd结果=re=" + re);
			}

		} catch (SQLException e) {
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (pst1 != null) {
				pst1.close();
				pst1 = null;
			}
			if (pst2 != null) {
				pst2.close();
				pst2 = null;
			}
			if (conn != null) {
				conn.close();
				conn = null;
			}
		}
		return re;

	}

}
