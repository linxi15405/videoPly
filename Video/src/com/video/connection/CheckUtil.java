package com.video.connection;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.Globals;

public class CheckUtil {

	public static Logger logger = Logger.getLogger(CheckUtil.class);

	public static boolean checkNull(String str) {
		if (str != null && !"".equals(str) && !"null".equals(str) && str != ""
				&& !str.equals(null) && str != "null") {
			return true;
		}
		return false;
	}

	// 检查是否等于-11,-1 是否可用
	public static boolean checkUseful(String str) {
		if (str == "-1" || str.trim().equals("-11") || str.trim().equals("-1")
				|| str == "-11") {
			return false;
		} else {
			return true;
		}
	}

	public static String UrlEncode(String str) throws Exception {
		try {
			if (!checkNull(str)) {
				return null;
			}
			String info = java.net.URLEncoder.encode(str, "utf-8");
			return info;
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}

	public static String UrlDecode(String str) throws Exception {
		try {
			if (!checkNull(str)) {
				return null;
			}
			String info = java.net.URLDecoder.decode(str, "utf-8");
			return info;
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}

	public static String international(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Locale enLocale = (Locale) session.getAttribute(Globals.LOCALE_KEY);
		ResourceBundle bundle = ResourceBundle.getBundle(
				"com.yourcompany.struts.ApplicationResources", enLocale);
		String message = bundle.getString("wzdxgjl");
		return message;

	}
}
