package com.video.connection;

import com.video.exception.LogException;

import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

@SuppressWarnings("unchecked")
public class DataMember extends DBMember {

	private static Logger logger = Logger.getLogger(DataMember.class);

	public DataMember() {

	}

	/**
	 * 执行查询语句
	 * 
	 * @param stmt
	 * @param sql
	 * @param o
	 * @return
	 * @throws SQLException
	 */
	public ResultSet executeQuery(PreparedStatement stmt, String sql, Object[] o)
			throws SQLException {
		ResultSet result = null;
		try {
			if (o != null && o.length > 0) {
				for (int i = 0; i < o.length; i++) {
					stmt.setObject(i + 1, o[i]);
				}
			}
			result = stmt.executeQuery();
			return result;
		} catch (SQLException me) {
			throw me;
		}
	}

	/**
	 * 带参数 select语句
	 * 
	 * @param sql
	 * @param objs
	 * @return
	 * @throws SQLException
	 */
	public <T> T getData(String jndi, String sql, Object[] objs, Class<T> clazz)
			throws Exception {
		ResultSet result = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = getConnectByJNDI(jndi);
			stmt = conn.prepareStatement(sql);
			result = this.executeQuery(stmt, sql, objs);
			ArrayList<T> list = ResultSetToData.getDataByClassFiled(result,
					clazz);
			if (list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (Exception e) {
			throw e;
		} finally {
			result = null;
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				LogException.print(e, "getData conn error");
			}
		}
	}

	/**
	 * 带参数的select
	 * 
	 * @param sql
	 * @param objs
	 * @return
	 */
	public <T> ArrayList<T> getDataList(String jndi, String sql, Object[] objs,
			Class<T> clazz) throws Exception {
		ResultSet result = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = getConnectByJNDI(jndi);
			stmt = conn.prepareStatement(sql);
			result = this.executeQuery(stmt, sql, objs);
			ArrayList<T> list = ResultSetToData.getDataByClassFiled(result,
					clazz);
			return list;
		} catch (Exception e) {
			throw e;
		} finally {
			result = null;
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				LogException.print(e, "getData conn error");
			}
		}
	}
}
