package com.video.connection;

/**
 * Created by shiji-cx on 2017/7/11.
 */
import java.lang.reflect.Field;

public class TransformToSql {
	/**
	 * 通过反射机制生成插入sql语句
	 *
	 * @param clazz
	 * @return
	 */
	public static String transformToInsertSql(Class<?> clazz) {
		StringBuffer bufferFront = new StringBuffer();
		StringBuffer bufferLast = new StringBuffer();
		String tableName = clazz.getSimpleName();
		bufferFront.append("insert into "
				+ tableName.substring(0, 1).toLowerCase()
				+ tableName.substring(1, tableName.length()) + "(");
		bufferLast.append(" values(");
		// 通过class得到所有的属性不受访问控制符空值
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			bufferFront.append(field.getName() + ",");
			bufferLast.append("?,");
		}
		bufferFront.delete(bufferFront.length() - 1, bufferFront.length());
		bufferLast.delete(bufferLast.length() - 1, bufferLast.length());
		bufferFront.append(")");
		bufferLast.append(")");
		bufferFront.append(bufferLast);

		return bufferFront.toString();
	}

	/**
	 * 通过反射机制生成通用的更新语句
	 *
	 * @param clazz
	 * @return
	 */
	public static String transformToUpdateSql(Class<?> clazz) {

		StringBuffer bufferFront = new StringBuffer();
		StringBuffer bufferLast = new StringBuffer();
		String tableName = clazz.getSimpleName();
		bufferFront.append("update " + tableName.substring(0, 1).toLowerCase()
				+ tableName.substring(1, tableName.length()) + " set ");
		bufferLast.append(" where id=?");
		// 通过class得到所有的属性不受访问控制符空值
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if (field.getName().equals("id"))
				continue;
			bufferFront.append(field.getName() + "=?,");
		}
		bufferFront.delete(bufferFront.length() - 1, bufferFront.length());
		bufferFront.append(bufferLast);
		return bufferFront.toString();
	}

}