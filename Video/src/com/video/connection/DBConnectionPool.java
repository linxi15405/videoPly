package com.video.connection;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.psc.db.ConnectionPool;
import com.video.bean.DBSetting;
import com.video.exception.LogException;

@SuppressWarnings("unchecked")
public class DBConnectionPool {

	private static Logger logger = Logger.getLogger(DBConnectionPool.class);

	public static Map<String, ConnectionPool> poolmap = new HashMap<String, ConnectionPool>();// 连接池配置Map(未用)

	public static Map<String, ConnectionPool> poolmap1 = new HashMap<String, ConnectionPool>();// 连接池配置Map(在用)

	/**
	 * 初始化 连接池
	 * 
	 * @param poolname
	 *            连接池名称
	 * @param usrname
	 *            数据库帐号
	 * @param password
	 *            数据库密码
	 * @param url
	 *            连接url
	 */
	public static void initConnectionPool(String poolname, String usrname,
			String password, String url, String drvername) {

		ConnectionPool pool = null;
		// 180000毫秒数
		pool = new ConnectionPool(poolname, 5, 10, 180000, url, usrname,
				password, drvername);
		poolmap.put(poolname, pool);

	}

	public static void initConnectionPool(Map map) {
		// 初始化 连接池
		if (map != null) {
			Set set = map.entrySet();
			Iterator i = set.iterator();
			while (i.hasNext()) {
				Map.Entry e = (Map.Entry) i.next();
				DBSetting db = (DBSetting) e.getValue();

				String poolName = db.getPoolname();
				String dbUrl = db.getDburl();
				String dbusername = db.getUsername();
				String dbPwd = db.getUserpassword();
				String dbDriver = db.getDrivername();

				logger.info(poolName + " = " + dbUrl);

				// System.out.println("初始化 "+poolName+" 连接池.....");
				ConnectionPool Member_pool = new ConnectionPool(poolName, 5,
						10, 180000, dbUrl, dbusername, dbPwd, dbDriver);
				poolmap1.put(poolName, Member_pool);
			}
		}
	}

	/**
	 * 创造单个连接池
	 */
	public static ConnectionPool bulidPool(DBSetting db) throws Exception {
		try {
			String poolName = db.getPoolname();
			String dbUrl = db.getDburl();
			String dbusername = db.getUsername();
			String dbPwd = db.getUserpassword();
			String dbDriver = db.getDrivername();
			ConnectionPool pool = new ConnectionPool(poolName, 5, 10, 180000,
					dbUrl, dbusername, dbPwd, dbDriver);
			poolmap1.put(poolName, pool);
			return pool;
		} catch (Exception e) {
			LogException.print(e, "重置" + db.getPoolname() + "连接池异常");
			throw e;
		}
	}

	/**
	 * 获得一个连接
	 * 
	 * @param poolname
	 *            连接池名
	 * @return
	 */
	public static Connection getConnection(String poolname) {
		ConnectionPool p = poolmap.get(poolname);
		try {
			if (p != null) {
				return p.getConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 判断连接池是否存在
	 * 
	 * @param poolname
	 * @return
	 */
	public static boolean check_PoolExist(String poolname) {
		ConnectionPool p = poolmap.get(poolname);
		if (p != null) {
			// System.out.println("poolname="+p.getName());
			return true;
		} else
			return false;
	}

	public static ConnectionPool getPool(String areaid) {
		// if(areaid!=null && !"".equals(areaid)){
		return poolmap.get(areaid);
		// }
	}

	/**
	 * 获得连接池
	 */
	public static ConnectionPool getPoolBypoolname(String poolname) {
		if (poolname == null || "".equals(poolname) || "null".equals(poolname)) {
			return null;
		}
		if (poolmap1 == null) {
			return null;
		}
		if (poolmap1.get(poolname) != null) {
			return poolmap1.get(poolname);
		} else {
			return null;
		}
	}

	/**
	 * 判断连接池是否存在
	 */
	public static boolean existPool(String poolname) throws Exception {
		try {
			ConnectionPool p = poolmap1.get(poolname);
			if (p != null) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			throw e;
		}
	}

}
