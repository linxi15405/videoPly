package com.video.connection;

/**
 * Created by shiji-cx on 2017/7/11.
 */
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ResultSetToData {
	/**
	 * 根据clazz和ResultSet返回对象(这个对象里的全部属性)集合
	 * 
	 * @param rs
	 * @param clazz
	 * @return
	 */
	public static <T> ArrayList<T> getDataByClassFiled(ResultSet rs,
			Class<T> clazz) throws Exception {
		ArrayList<T> domains = new ArrayList<>();
		try {
			// 获得自己的属性
			Field[] fields = clazz.getDeclaredFields();
			while (rs.next()) {
				T vo = clazz.newInstance();
				// 历遍所有的属性
				for (Field field : fields) {
					String methodName = getSetterMethodName(field.getName());
					Method method = clazz
							.getMethod(methodName, field.getType());
					invokeMothod(rs, field, method, vo);
				}
				domains.add(vo);
			}
		} catch (InstantiationException | IllegalAccessException
				| SecurityException | SQLException | NoSuchMethodException e) {
			throw e;
		}
		return domains;
	}

	/**
	 * 根据属性名得到set的方法名
	 *
	 * @param name
	 * @return
	 */
	private static String getSetterMethodName(String name) {
		String begin = name.substring(0, 1).toUpperCase();
		String mothodName = "set" + begin + name.substring(1, name.length());
		return mothodName;
	}

	/**
	 * 根据ResultSet和Field的getName从结果集取出
	 *
	 * @param rs
	 * @param field
	 * @param method
	 * @param object
	 */
	private static void invokeMothod(ResultSet rs, Field field, Method method,
			Object object) throws Exception {
		try {
			Object o = rs.getObject(field.getName());
			method.invoke(object, o);
		} catch (IllegalAccessException e) {
			throw e;
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (InvocationTargetException e) {
			throw e;
		} catch (SQLException e) {
			throw e;
		}
	}

}